<?php
/**
 * Addon for Cockpit CMS to have a custom publish button to trigger webhook
 * 
 * @see       https://gitlab.com/0to100/cockpit-publish-button
 * @see       https://github.com/agentejo/cockpit/
 * 
 * @version   1.0.0
 * @author    Marcel Pfeifer
 * @license   MIT
 */

    $app->on('admin.dashboard.header', function(){
        echo '<form method="post">
		<input type="button" id="publishButton" class="uk-badge uk-margin-small-right" value="Veröffentlichen"/>
		</form>';
    });

	function publish() {
		echo "Your test function on button click is working";
		$this->app->trigger('publish', [$collection['name'], &$entries]);
	}

	if(array_key_exists('publishButton',$_POST)){
		publish();
	}

?>